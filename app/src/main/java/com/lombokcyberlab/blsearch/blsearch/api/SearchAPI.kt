package com.lombokcyberlab.blsearch.blsearch.api

import com.lombokcyberlab.blsearch.blsearch.model.FavoriteResponse
import com.lombokcyberlab.blsearch.blsearch.model.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by ucha on 07/12/17.
 */
interface SearchAPI {
    companion object {
        val API_URL = "https://api.bukalapak.com"
        val API_VERSION = "v2"
        val BASE_URL = "$API_URL/$API_VERSION/"
    }

    @GET("omniscience/v2.json")
    fun search(@Query("user") user:String, @Query("key") apiKey:String, @Query("word") queryString:String):Call<SearchResponse>


    @GET("top_keywords/index.json")
    fun getFav():Call<FavoriteResponse>


}