package com.lombokcyberlab.blsearch.blsearch.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.lombokcyberlab.blsearch.blsearch.R
import com.lombokcyberlab.blsearch.blsearch.api.contactors.FavoriteContactorImpl
import com.lombokcyberlab.blsearch.blsearch.presenter.impl.FavoritePresenterImpl
import com.lombokcyberlab.blsearch.blsearch.presenter.impl.SearchPresenterImpl
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val userID = "3e995240926d49ac9ecf60b58ad62167"
    val apiKey = "b58bfa45d99e3f1406ff895553eb2d44"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        editKeyword.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0 == null || p0.isBlank()) {
                    loadFavorite()
                } else {
                    loadKeyword(p0.toString())
                }
            }

        })
        loadFavorite()
    }

    private fun loadFavorite() {
        FavoritePresenterImpl(recycler).execute()
    }

    private fun loadKeyword(query:String) {
        SearchPresenterImpl(apiKey, userID, query, recycler).execute()
    }

}
