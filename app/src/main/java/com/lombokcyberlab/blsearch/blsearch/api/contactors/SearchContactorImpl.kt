package com.lombokcyberlab.blsearch.blsearch.api.contactors

import com.lombokcyberlab.blsearch.blsearch.api.BaseContactor
import com.lombokcyberlab.blsearch.blsearch.model.SearchResponse
import com.lombokcyberlab.blsearch.blsearch.presenter.BasePresenter
import retrofit2.Call

/**
 * Created by ucha on 07/12/17.
 */
class SearchContactorImpl(val userID:String, val apiKey:String, val query:String, handler:BasePresenter<SearchResponse>) :
        BaseContactor<SearchResponse>(handler) {

    override fun prepareCaller(): Call<SearchResponse> {
        return api.search(user = userID, apiKey = apiKey, queryString = query)
    }
}