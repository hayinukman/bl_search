package com.lombokcyberlab.blsearch.blsearch.presenter.impl

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.lombokcyberlab.blsearch.blsearch.R
import com.lombokcyberlab.blsearch.blsearch.api.contactors.FavoriteContactorImpl
import com.lombokcyberlab.blsearch.blsearch.model.FavoriteResponse
import com.lombokcyberlab.blsearch.blsearch.presenter.BasePresenter

/**
 * Created by ucha on 07/12/17.
 */
class FavoritePresenterImpl(val recycler:RecyclerView) :BasePresenter<FavoriteResponse>(recycler.context) {
    init {
        contactor = FavoriteContactorImpl(this)
    }
    override fun onSuccess(data: FavoriteResponse?) {
        recycler.adapter = FavAdapter(data)
    }

    override fun onError(message: String, code: Int) {

    }

    override fun showProgress(show: Boolean) {

    }

    fun execute() {
        contactor?.execute()
    }


    inner class FavAdapter(var data:FavoriteResponse?):RecyclerView.Adapter<FavAdapter.FavHolder>() {

        override fun onBindViewHolder(holder: FavHolder?, position: Int) {
            if (position <= 0) {
                holder?.setData("Pencarian Populer")
            } else {
                holder?.setData(data?.top_keywords?.get(position-1) ?: "")
            }

        }

        override fun getItemCount(): Int = data?.top_keywords?.size ?: 1


        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): FavHolder {
            return when(viewType) {
                0 -> FavHolder(LayoutInflater.from(context).inflate(R.layout.group_item,parent,false))
                else -> FavHolder(LayoutInflater.from(context).inflate(R.layout.favorite_item,parent,false))
            }
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }


        inner class FavHolder(v:View): RecyclerView.ViewHolder(v) {
            fun setData(data:String) {
                (itemView as TextView).text = data
            }
        }
    }

}
