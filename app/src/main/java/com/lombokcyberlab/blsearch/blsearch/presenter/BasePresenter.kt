package com.lombokcyberlab.blsearch.blsearch.presenter

import android.content.Context
import com.lombokcyberlab.blsearch.blsearch.api.BaseContactor

/**
 * Created by ucha on 07/12/17.
 */
abstract class BasePresenter<T>(val context: Context) {
    var contactor:BaseContactor<T>? = null
    abstract fun onSuccess(data:T?)
    abstract fun onError(message:String, code:Int)
    abstract fun showProgress(show:Boolean = true)
}