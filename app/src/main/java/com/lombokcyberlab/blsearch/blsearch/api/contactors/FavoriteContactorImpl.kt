package com.lombokcyberlab.blsearch.blsearch.api.contactors

import com.lombokcyberlab.blsearch.blsearch.api.BaseContactor
import com.lombokcyberlab.blsearch.blsearch.model.FavoriteResponse
import com.lombokcyberlab.blsearch.blsearch.model.SearchResponse
import com.lombokcyberlab.blsearch.blsearch.presenter.BasePresenter
import retrofit2.Call

/**
 * Created by ucha on 07/12/17.
 */
class FavoriteContactorImpl(handler: BasePresenter<FavoriteResponse>) :
        BaseContactor<FavoriteResponse>(handler) {
    override fun prepareCaller(): Call<FavoriteResponse> {
        return api.getFav()
    }
}