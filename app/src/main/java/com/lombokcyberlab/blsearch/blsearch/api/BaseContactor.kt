package com.lombokcyberlab.blsearch.blsearch.api

import android.util.Log.e
import com.lombokcyberlab.blsearch.blsearch.presenter.BasePresenter
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by ucha on 07/12/17.
 */
abstract class BaseContactor<T>(val handler:BasePresenter<T>) {
    private val interceptor = HttpLoggingInterceptor()
    private val retrofit: Retrofit
    private val client: OkHttpClient
    protected val api: SearchAPI

    init {
        interceptor.level = HttpLoggingInterceptor.Level.BODY

//        val cf = CertificateFactory.getInstance("X.509")
//        val cert = handler.context.resources.openRawResource(R.raw.my_cert)
//        val ca: Certificate
//        try {
//            ca = cf.generateCertificate(cert)
//        } finally {
//            cert.close()
//        }

        // creating a KeyStore containing our trusted CAs
//        val keyStoreType = KeyStore.getDefaultType()
//        val keyStore = KeyStore.getInstance(keyStoreType)
//        keyStore.load(null, null)
//        keyStore.setCertificateEntry("ca", ca)

        // creating a TrustManager that trusts the CAs in our KeyStore
//        val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
//        val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
//        tmf.init(keyStore)

        // creating an SSLSocketFactory that uses our TrustManager
//        val sslContext = SSLContext.getInstance("TLS")
//        sslContext.init(null, tmf.getTrustManagers(), null)

        client = OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor)
                .readTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300,TimeUnit.SECONDS)
                .connectTimeout(300, TimeUnit.SECONDS)
//                .sslSocketFactory(sslContext.getSocketFactory())
//                .hostnameVerifier { s, sslSession ->
//                    e("SERVER", "verify server $s")
//                    "kqsapp.com".toLowerCase() == s
//                }
                .build()

        retrofit = Retrofit.Builder()
                .baseUrl(SearchAPI.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        api = retrofit.create(SearchAPI::class.java)
    }

    protected val callback: Callback<T> = object : Callback<T> {
        override fun onFailure(call: Call<T>?, t: Throwable?) {
            e("API::onFailure", "failure on retrofit call: ${t?.message}")
            handler.onError("${t?.message}", -1)
            handler.showProgress(false)
        }

        override fun onResponse(call: Call<T>?, response: Response<T>?) {
            handler.showProgress(false)
            e("API::onResponse", "Retrofit::onResponse -> code:${response?.code()} body:${response?.isSuccessful}")
            if (response?.isSuccessful ?: false) {
                e("API::onResponse", "Retrofit::onResponse -> code:${response?.code()} body:${response?.body()}")
                handler.onSuccess(response?.body())

            } else {
                handler.onError("${response?.errorBody()?.string()}", response?.code()?:-1)
            }
        }
    }

    abstract fun prepareCaller():Call<T>

    fun execute() {
        handler.showProgress()
        val call = prepareCaller()
        call.enqueue(callback)

    }

}