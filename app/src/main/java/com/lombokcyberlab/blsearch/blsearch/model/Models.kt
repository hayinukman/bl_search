package com.lombokcyberlab.blsearch.blsearch.model

/**
 * Created by ucha on 07/12/17.
 */



enum class Type {
    CATEGORY,
    USER,
    PRODUCT,
    FAV,
    GROUP,
    TEXT,
}
data class Category(

        val category: String,
        val url: String,
        val name: String,
        val id: Long,
        val type: Type = Type.CATEGORY
)

data class User(
        val id: Long,
        val name: String,
        val url: String,
        val img: String,
        val type: Type = Type.USER
)

data class Product(
        val id: Long,
        val img: String,
        val url: String,
        val name: String,
        val price: Long,
        val type: Type = Type.PRODUCT
)

data class FavoriteResponse(
        val status:String,
        val top_keywords:MutableList<String>,
        val message: Any?
)

data class SearchResponse(
        val psize:Long,
        val word:MutableList<String>,
        val category:MutableList<Category>,
        val user:MutableList<User>,
        val history:MutableList<Any>,
        val keyword:MutableList<Any>,
        val product:MutableList<Product>,
        val time:String
)

data class Item(
        val type: Type,
        val data:Any
)