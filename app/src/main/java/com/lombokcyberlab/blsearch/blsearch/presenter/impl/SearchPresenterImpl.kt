package com.lombokcyberlab.blsearch.blsearch.presenter.impl

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.lombokcyberlab.blsearch.blsearch.R
import com.lombokcyberlab.blsearch.blsearch.api.contactors.FavoriteContactorImpl
import com.lombokcyberlab.blsearch.blsearch.api.contactors.SearchContactorImpl
import com.lombokcyberlab.blsearch.blsearch.model.*
import com.lombokcyberlab.blsearch.blsearch.presenter.BasePresenter
import kotlinx.android.synthetic.main.barang_item.view.*

/**
 * Created by ucha on 07/12/17.
 */
class SearchPresenterImpl(val apiKey: String, val userID: String, val query: String, val recycler: RecyclerView) : BasePresenter<SearchResponse>(recycler.context) {

    init {
        contactor = SearchContactorImpl(userID, apiKey, query, this)
    }


    override fun onSuccess(data: SearchResponse?) {
        val dt = mutableListOf<Item>()
        dt.add(Item(Type.GROUP, "Kata Kunci"))
        data?.keyword?.forEach {
            dt.add(Item(Type.TEXT, it))
        }

        dt.add(Item(Type.GROUP, "Kategory"))
        data?.category?.forEach {
            dt.add(Item(Type.CATEGORY, it.category))
        }

        dt.add(Item(Type.GROUP, "Barang"))
        data?.product?.forEach {
            dt.add(Item(Type.PRODUCT, it))
        }
        dt.add(Item(Type.GROUP, "Riwayat Barang"))
        data?.history?.forEach {
            dt.add(Item(Type.TEXT, it))
        }
        dt.add(Item(Type.GROUP, "Riwayat Kata Kunci"))
        data?.keyword?.forEach {
            dt.add(Item(Type.TEXT, it))
        }

        dt.add(Item(Type.GROUP, "Pelapak"))
        data?.user?.forEach {
            Log.e("User", "${it}")
            dt.add(Item(Type.USER, it))
        }

        recycler.adapter = SearchAdapter(dt)

    }

    override fun onError(message: String, code: Int) {

    }

    override fun showProgress(show: Boolean) {

    }

    fun execute() {
        contactor?.execute()
    }

    inner class SearchAdapter(val data: MutableList<Item>) : RecyclerView.Adapter<SearchAdapter.BaseHolder>() {
        override fun onBindViewHolder(holder: BaseHolder?, position: Int) {
            val dt = data.get(position)
            when (dt.type) {
                Type.GROUP -> (holder as GroupHolder).setData("${dt.data}")
                Type.TEXT -> (holder as TextHolder).setData("${dt.data}")
                Type.PRODUCT -> (holder as ProductHolder).setData(dt.data as Product)
                Type.USER -> (holder as UserHolder).setData(dt.data as User)
                else -> (holder as TextHolder).setData("${dt.data}")
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BaseHolder {
            return when (viewType) {
                Type.GROUP.ordinal -> GroupHolder(LayoutInflater.from(context).inflate(R.layout.group_item, parent, false))
                Type.TEXT.ordinal -> TextHolder(LayoutInflater.from(context).inflate(R.layout.favorite_item, parent, false))
                Type.CATEGORY.ordinal -> TextHolder(LayoutInflater.from(context).inflate(R.layout.favorite_item, parent, false))
                Type.PRODUCT.ordinal -> ProductHolder(LayoutInflater.from(context).inflate(R.layout.barang_item, parent, false))
                Type.USER.ordinal -> UserHolder(LayoutInflater.from(context).inflate(R.layout.barang_item, parent, false))
                else -> TextHolder(LayoutInflater.from(context).inflate(R.layout.favorite_item, parent, false))
            }

        }

        override fun getItemViewType(position: Int): Int {
            return data[position].type.ordinal
        }

        override fun getItemCount(): Int = data.size


        inner abstract class BaseHolder(v: View) : RecyclerView.ViewHolder(v) {

        }

        inner class ProductHolder(v: View) : BaseHolder(v) {
            fun setData(data: Product) {
                Glide.with(context).load(data.img).into(itemView.image)
                itemView.name.text = data.name
                itemView.prices.text = "Rp. ${data.price}"
            }
        }

        inner class UserHolder(v: View) : BaseHolder(v) {
            fun setData(data: User) {
                Glide.with(context).load(data.img).into(itemView.image)
                itemView.name.text = data.name
                itemView.prices.visibility = View.GONE
            }
        }


        inner class TextHolder(v: View) : BaseHolder(v) {
            fun setData(data: String) {
                (itemView as TextView).text = data
            }

        }

        inner class GroupHolder(v: View) : BaseHolder(v) {
            fun setData(data: String) {
                (itemView as TextView).text = data
            }

        }


    }

}